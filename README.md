## Ansible

#### What is it
Ansible é uma ferramenta de automação utilizada para gerência de configuração, deploy de aplicações, e provisionamento de infraestrutura. extremamente utlizado em ambientes com centenas de servidores. A comunicação é feita através do protocolo *SSH* sem a necessidade de agentes instalados no hosts remotos.

#### Main concepts
- [ ] *Ad-hoc*: é um comando ou automação fornecida pelo Ansible para gerenciamento de configuração através da CLI do LINUX.
- [ ] *Playbooks*: é um conjunto de tasks ou tarefas descritas no formato *YML*.
- [ ] *Roles*: é uma estrutura de diretórios organizada de forma lógica para utilização em projetos complexos.
- [ ] *inventory file*: arquivo utilizado para gerenciamento dos hosts que o Ansible vai afetar

#### How it helps
Basicamente o *Ansible* ajuda a manter todo o gerenciamento de configuração da sua infraestrutura de forma simples e veloz. A ideia do *Ansible* é automatizar em grande escala servidores, ativos de redes usando a abordagem de *IaC* (Infrastructure as Code).

